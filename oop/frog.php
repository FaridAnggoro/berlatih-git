<?php

require_once('animal.php');

class frog extends animal
{
    public $name = "buduk";
    public $legs = 4;
    public $cold_blooded = "no";

    public function jump()
    {
        return "Jump : Hop Hop";
    }
}
