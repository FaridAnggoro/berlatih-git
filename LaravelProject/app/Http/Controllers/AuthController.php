<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('homepage/register');
    }

    public function welcome(Request $request)
    {
        $namaDepan = $request['firstname'];
        $namaBelakang = $request['lastname'];
        return view('homepage.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
