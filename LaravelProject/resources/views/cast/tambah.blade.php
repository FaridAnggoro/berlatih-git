@extends('layout.master')

@section('judul')
<h1>
    Halaman Form
</h1>
@endsection

@section('title')
<h3>
    Membuat Data Pemain Film Baru
</h3>
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>nama</label>
        <input name="nama" type="text" value="{{old('nama')}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>umur</label>
        <input name="umur" type="text" value="{{old('umur')}}" class=" form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">"{{old('bio')}}"</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection