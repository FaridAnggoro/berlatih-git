@extends('layout.master')

@section('judul')
<h1>
    Halaman Tampil
</h1>
@endsection

@section('title')
<h3>
    menampilkan list data para pemain film
</h3>
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary my-3">Tambah</a>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">nama</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item )
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{$item->nama}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <h1>Data Kosong</h1>
        @endforelse

    </tbody>
</table>
@endsection