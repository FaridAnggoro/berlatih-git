@extends('layout.master')

@section('judul')
<h1>
    Halaman Detail
</h1>
@endsection

@section('title')
<h3>
    menampilkan detail data pemain film
</h3>
@endsection

@section('content')
<h1 class="text-primary">{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary my-2">Kembali</a>
@endsection