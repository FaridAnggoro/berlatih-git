@extends('layout.master')

@section('judul')
<h1>
    Halaman Edit
</h1>
@endsection

@section('title')
<h3>
    menampilkan form untuk edit pemain film
</h3>
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama</label>
        <input name="nama" type="text" value="{{old('nama', $cast->nama)}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input name="umur" type="text" value="{{old('umur', $cast->umur)}}" class=" form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">"{{old('bio', $cast->bio)}}"</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection