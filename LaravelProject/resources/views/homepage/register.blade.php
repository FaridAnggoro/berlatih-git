@extends('layout.master')

@section('judul')
<h1>
    Halaman Buat Account Baru!
</h1>
@endsection

@section('title')
<h3>
    Sign Up Form
</h3>
@endsection

@section('content')
<!-- input form -->
<form action="/welcome" method="POST">
    @csrf
    <label>
        First name:
    </label>
    <br>
    <br>
    <input type="text" name="firstname">
    <br>
    <br>
    <label>
        Last name:
    </label>
    <br>
    <br>
    <input type="text" name="lastname">
    <br>
    <br>
    <label>
        Gender:
    </label>
    <br>
    <br>
    <input type="radio">Male
    <br>
    <input type="radio">Female
    <br>
    <input type="radio">Other

    <!-- Akhir Input Form -->
    <br>
    <br>
    <!-- Dropdown -->
    <label>
        Nationality:
    </label>
    <br>
    <br>
    <select name="Nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="Indonesian">Singapore</option>
        <option value="Indonesian">Malaysia</option>
        <option value="Indonesian">Australia</option>
    </select>
    <!-- Akhir Dropdown -->
    <br>
    <br>
    <!-- Input Form Kedua -->
    <label>
        Language Spoken
    </label>
    <br>
    <br>
    <input type="checkbox" name="Language">Bahasa Indonesia
    <br>
    <input type="checkbox" name="Language">English
    <br>
    <input type="checkbox" name="Language">Other
    <!-- Akhir Input Form -->
    <br><br>
    <!-- Text Area -->
    <label>
        Bio:
    </label>
    <br>
    <br>
    <textarea name="message" cols="30" rows="10">
        </textarea>
    <br>
    <br>
    <input type="submit" value="Sign Up">
</form>
<!-- Akhir Text Area -->
@endsection