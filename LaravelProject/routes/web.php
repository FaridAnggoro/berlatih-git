<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Halaman Homepage
Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);
// Akhir Halaman Homepage

// Table
Route::get('/table', function () {
    return view('homepage.table');
});
// Akhir Table

// Data Table
Route::get('/data-table', function () {
    return view('homepage.data-table');
});
// Akhir Table

// CRUD Cast
// Route untuk mengarah ke form cast
Route::get('/cast/create', [CastController::class, 'create']);

// route menyimpan data baru
Route::post('/cast', [CastController::class, 'store']);

// Route read
Route::get('/cast', [CastController::class, 'index']);

// detail cast berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// update 
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

// update data berdasarkan id
Route::put('/cast/{$cast_id}', [CastController::class, 'update']);

// Delete
Route::delete('/cast/{$cast_id}', [CastController::class, 'destroy']);
